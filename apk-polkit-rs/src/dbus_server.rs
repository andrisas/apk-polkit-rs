/*
    Copyright: 2020-2024 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

use crate::{
    apk_package_partial::{ApkPackagePartial, ToPartial},
    change_repository_actions::ChangeRepositoryActions,
    details_flags::DetailsFlags,
    error,
};

use apk_tools::i18n::i18n_f;
use apk_tools::{ApkDatabase, ApkRepository};
use enumflags2::BitFlags;
use serde::Serialize;
use std::process::Command;
use zbus::{self, message::Header, Connection};

#[derive(Default)]
pub struct DBusServer {
    allow_untrusted_repos: bool,
    additional_repo_uri: Option<String>,
    root: Option<String>,
}

#[derive(Debug, Serialize, zvariant::Type, PartialEq, Eq)]
#[zvariant(signature = "(bss)")]
pub struct ApkRepositoryDBus(ApkRepository);

impl DBusServer {
    #[cfg(test)]
    pub fn new(
        allow_untrusted_repos: bool,
        additional_repo_uri: Option<String>,
        root: Option<String>,
    ) -> Self {
        Self {
            allow_untrusted_repos,
            additional_repo_uri,
            root,
        }
    }

    async fn check_polkit_auth(
        method_name: &str,
        header: &Header<'_>,
        connection: &Connection,
    ) -> Result<(), zbus::fdo::Error> {
        use std::collections::HashMap;
        use zbus_polkit::policykit1::{AuthorityProxy, Subject};

        let proxy = AuthorityProxy::new(connection).await?;
        let subject = match Subject::new_for_message_header(header) {
            Ok(o) => o,
            Err(e) => {
                return Err(zbus::fdo::Error::AuthFailed(i18n_f(
                    "Failed to determine subject: {}",
                    &[&e.to_string()],
                )))
            }
        };
        let mut check_auth_details = HashMap::new();
        check_auth_details.insert("polkit.gettext_domain", "apk-polkit-rs");
        let res = proxy
            .check_authorization(
                &subject,
                &format!("dev.Cogitri.apkPolkit2.{method_name}"),
                &check_auth_details,
                enumflags2::BitFlags::empty(),
                "",
            )
            .await
            .map_err(|e| {
                zbus::fdo::Error::AuthFailed(i18n_f(
                    "Failed to query for authentication: {}",
                    &[&e.to_string()],
                ))
            })?;
        if res.is_authorized {
            Ok(())
        } else {
            Err(zbus::fdo::Error::AuthFailed(i18n_f(
                "Access for operation {} was denied",
                &[method_name],
            )))
        }
    }

    fn create_database(
        &self,
        read_only: bool,
        repositories_file: Option<String>,
    ) -> Result<ApkDatabase, apk_tools::Error> {
        ApkDatabase::new(
            read_only,
            self.additional_repo_uri.clone(),
            self.root.clone(),
            repositories_file,
            self.allow_untrusted_repos,
            false,
        )
    }
}

#[zbus::interface(name = "dev.Cogitri.apkPolkit2")]
impl DBusServer {
    async fn add_packages(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        packages: Vec<&str>,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("AddPackages", &header, connection).await?;
        let mut db = self.create_database(false, None)?;
        #[cfg(test)]
        db.no_extract_chown();
        db.add_packages(packages)?;
        Ok(())
    }

    async fn add_repository(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        url: &str,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("AddRepository", &header, connection).await?;
        let mut repos = ApkDatabase::get_repositories(self.root.as_deref())?;
        if !repos.iter().any(|r| r.url == url) {
            repos.push(ApkRepository::new(true, None, url.to_string()));
            ApkDatabase::set_repositories(&repos, self.root.as_deref()).await?;
        }
        Ok(())
    }

    async fn delete_packages(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        packages: Vec<&str>,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("DeletePackage", &header, connection).await?;
        let mut db = self.create_database(false, None)?;
        db.delete_packages(packages)?;
        Ok(())
    }

    async fn get_packages_details(
        &self,
        requested_packages: Vec<&str>,
        requested_properties: BitFlags<DetailsFlags>,
    ) -> error::Result<Vec<ApkPackagePartial>> {
        let mut db = self.create_database(false, None)?;
        Ok(db
            .get_packages_details(requested_packages)
            .to_partial(requested_properties))
    }

    async fn list_repositories(&self) -> error::Result<Vec<ApkRepositoryDBus>> {
        Ok(ApkDatabase::get_repositories(self.root.as_deref())?
            .drain(..)
            .map(ApkRepositoryDBus)
            .collect())
    }

    async fn list_upgradable_packages(
        &self,
        requested_properties: BitFlags<DetailsFlags>,
    ) -> error::Result<Vec<ApkPackagePartial>> {
        let mut db = self.create_database(true, None)?;
        Ok(db
            .list_upgradable_packages()
            .map(|s| s.to_partial(requested_properties))?)
    }

    async fn remove_repository(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        url: &str,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("RemoveRepository", &header, connection).await?;
        let mut repos = ApkDatabase::get_repositories(self.root.as_deref())?;
        repos.retain(|r| r.url != url);
        ApkDatabase::set_repositories(&repos, self.root.as_deref()).await?;
        Ok(())
    }

    async fn change_repository(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        url: &str,
        action: i32,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("ChangeRepository", &header, connection).await?;

        let action = ChangeRepositoryActions::try_from(action)?;
        let mut repos = ApkDatabase::get_repositories(self.root.as_deref())?;
        if let Some(r) = repos.iter_mut().find(|r| r.url == url) {
            r.enabled = action == ChangeRepositoryActions::Enable;
        }
        ApkDatabase::set_repositories(&repos, self.root.as_deref()).await?;
        Ok(())
    }

    async fn search_files_owners(
        &self,
        paths: Vec<&str>,
        requested_properties: BitFlags<DetailsFlags>,
    ) -> error::Result<Vec<ApkPackagePartial>> {
        let mut db = self.create_database(true, None)?;
        let mut vec = Vec::with_capacity(paths.len());
        for path in paths {
            vec.push(
                db.search_file_owner(path)
                    .map_or_else(ApkPackagePartial::empty, |p| {
                        ApkPackagePartial::from_apk_package(p, requested_properties)
                    }),
            )
        }
        Ok(vec)
    }

    async fn update_repositories(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("UpdateRepositories", &header, connection).await?;
        let mut db = self.create_database(false, None)?;
        db.update_repositories()?;
        Command::new("alpine-appstream-downloader").output()?;
        Ok(())
    }

    async fn upgrade_packages(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        pkgnames: Vec<&str>,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("UpgradePackages", &header, connection).await?;
        let mut db = self.create_database(false, None)?;
        #[cfg(test)]
        db.no_extract_chown();
        db.upgrade_packages(&pkgnames)?;
        Ok(())
    }

    async fn cache_packages(
        &mut self,
        #[zbus(header)] header: Header<'_>,
        #[zbus(connection)] connection: &Connection,
        pkgnames: Vec<&str>,
        repositories_file: &str,
        cache_world: bool,
    ) -> error::Result<()> {
        DBusServer::check_polkit_auth("CachePackage", &header, connection).await?;
        let mut db = self.create_database(false, Some(repositories_file.to_string()))?;
        db.update_repositories()?;
        db.cache_packages(pkgnames, cache_world)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::{
        apk_package_partial::ApkPackagePartial, dbus_server::ChangeRepositoryActions,
        details_flags::DetailsFlags,
    };

    use super::DBusServer;
    use apk_tools::{ApkRepository, PackageState};
    use enumflags2::BitFlag;
    use std::io::Write;
    use std::path::{Path, PathBuf};
    use std::process::Command;
    use std::sync::Once;
    use tokio::{
        fs::{create_dir_all, File},
        io::AsyncWriteExt,
    };
    use zbus::*;

    static INIT_LOGGER: Once = Once::new();

    fn start_fs_test() -> (String, String, tempfile::TempDir) {
        INIT_LOGGER.call_once(|| {
            fern::Dispatch::new()
                .format(move |out, message, record| {
                    out.finish(format_args!("[{}] {}", record.level(), message,))
                })
                .chain(std::io::stdout())
                .apply()
                .unwrap();
        });

        let temp_dir = tempfile::tempdir().unwrap();
        let mut repo_build_script_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        repo_build_script_path.pop();
        let repo_build_script_path = repo_build_script_path
            .join("tests")
            .join("build_repo.sh")
            .to_str()
            .unwrap()
            .to_string();

        let apk_root_dir = temp_dir.as_ref().join("apk_root");
        let abuild_build_dir = temp_dir.as_ref().join("abuild_root");

        let abuild_cmd = Command::new(repo_build_script_path)
            .args(&[&apk_root_dir, &abuild_build_dir])
            .env("APK", "apk")
            .output()
            .expect("Failed to execute abuild!");

        if !abuild_cmd.status.success() {
            println!("Running abuild failed!");
            std::io::stdout().write(&abuild_cmd.stdout).unwrap();
            std::io::stdout().write(&abuild_cmd.stderr).unwrap();
            panic!("Running abuild failed!");
        }

        unsafe { apk_tools_sys::apk_flags = apk_tools_sys::APK_ALLOW_UNTRUSTED };

        (
            apk_root_dir.to_str().unwrap().to_string(),
            abuild_build_dir
                .join("abuilds")
                .to_str()
                .unwrap()
                .to_string(),
            temp_dir,
        )
    }

    fn build_additional_package(pkgname: &str, repo_dir: &str, root_dir: &str) {
        let repo_path = Path::new(repo_dir).join(pkgname);

        //    APK="$APK --allow-untrusted --root $1" SUDO_APK="abuild-apk --root $1" REPODEST="$2"
        let abuild_cmd = Command::new("abuild")
            .args(&[
                "-F",
                "clean",
                "unpack",
                "prepare",
                "build",
                "rootpkg",
                "update_abuildrepo_index",
            ])
            .env("APK", format!("apk --allow-untrusted --root {}", root_dir))
            .env("SUDO_APK", format!("abuild-apk --root {}", root_dir))
            .env("REPODEST", &Path::new(repo_dir).parent().unwrap())
            .env(
                "ABUILD_USERDIR",
                &Path::new(repo_dir).parent().unwrap().join("abuildUserDir"),
            )
            .current_dir(repo_path)
            .output()
            .expect("Failed to execute abuild!");

        if !abuild_cmd.status.success() {
            println!("Running abuild failed!");
            std::io::stdout().write_all(&abuild_cmd.stdout).unwrap();
            std::io::stdout().write_all(&abuild_cmd.stderr).unwrap();
            panic!("Running abuild failed!");
        }
    }

    fn get_message() -> zbus::message::Message {
        let mut builder =
            zbus::message::Message::method_call("/dev/Cogitri/apkPolkit2", "AddPackages").unwrap();
        builder = builder.sender(":org.donot").unwrap();
        builder.build(&()).unwrap()
    }

    #[tokio::test]
    async fn test_add() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .add_packages(
                get_message().header(),
                &connection,
                vec!["test-a", "test-b"],
            )
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.0\n");
        let output = Command::new(format!("{}/usr/bin/test-b", root)).output().expect("Running test-b executable failed; apparently installing the test-b package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-b-1.0\n");

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_add_unknown_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        assert!(server
            .add_packages(get_message().header(), &connection, vec!["unknown"])
            .await
            .is_err());

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_delete() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .add_packages(
                get_message().header(),
                &connection,
                vec!["test-a", "test-b"],
            )
            .await
            .unwrap();
        let a_path = std::path::Path::new(&root)
            .join("usr")
            .join("bin")
            .join("test-a");
        assert!(a_path.exists());
        let b_path = std::path::Path::new(&root)
            .join("usr")
            .join("bin")
            .join("test-b");
        assert!(b_path.exists());
        server
            .delete_packages(
                get_message().header(),
                &connection,
                vec!["test-a", "test-b"],
            )
            .await
            .unwrap();
        assert!(!a_path.exists());
        assert!(!b_path.exists());

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_delete_unknown_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .delete_packages(get_message().header(), &connection, vec!["test-a"])
            .await
            .unwrap();
        let exe_path = std::path::Path::new(&root)
            .join("usr")
            .join("bin")
            .join("test-a");
        assert!(!exe_path.exists());

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_repo_add() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        let repo_file_path = Path::new(&root)
            .join("etc")
            .join("apk")
            .join("repositories");
        create_dir_all(repo_file_path.parent().unwrap())
            .await
            .unwrap();
        let mut repo_file = File::create(&repo_file_path).await.unwrap();
        repo_file.write(b"https://alpine.global.ssl.fastly.net/alpine/edge/main\n#https://alpine.global.ssl.fastly.net/alpine/edge/testing\nhttps://alpine.global.ssl.fastly.net/alpine/edge/community\n").await.unwrap();
        server
            .add_repository(
                get_message().header(),
                &connection,
                "https://alpine.global.ssl.fastly.net/alpine/edge/testing",
            )
            .await
            .unwrap();
        let repos = server.list_repositories().await.unwrap();
        let repo_file_path_s = repo_file_path.to_str().map(|s| s.to_string());
        assert!(
            repos[0].0
                == ApkRepository::new(
                    true,
                    repo_file_path_s.clone(),
                    "https://alpine.global.ssl.fastly.net/alpine/edge/main".to_string(),
                )
        );
        assert!(
            repos[1].0
                == ApkRepository::new(
                    false,
                    repo_file_path_s.clone(),
                    "https://alpine.global.ssl.fastly.net/alpine/edge/testing".to_string(),
                ),
        );
        assert!(
            repos[2].0
                == ApkRepository::new(
                    true,
                    repo_file_path_s.clone(),
                    "https://alpine.global.ssl.fastly.net/alpine/edge/community".to_string(),
                ),
        );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_repo_remove() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        let repo_file_path = Path::new(&root)
            .join("etc")
            .join("apk")
            .join("repositories");
        create_dir_all(repo_file_path.parent().unwrap())
            .await
            .unwrap();
        let mut repo_file = File::create(&repo_file_path).await.unwrap();
        repo_file.write(b"https://alpine.global.ssl.fastly.net/alpine/edge/main\n#https://alpine.global.ssl.fastly.net/alpine/edge/testing\nhttps://alpine.global.ssl.fastly.net/alpine/edge/community\n").await.unwrap();
        server
            .remove_repository(
                get_message().header(),
                &connection,
                "https://alpine.global.ssl.fastly.net/alpine/edge/community",
            )
            .await
            .unwrap();
        let repos = server.list_repositories().await.unwrap();
        let repo_file_path_s = repo_file_path.to_str().map(|s| s.to_string());
        assert!(
            repos[0].0
                == ApkRepository::new(
                    true,
                    repo_file_path_s.clone(),
                    "https://alpine.global.ssl.fastly.net/alpine/edge/main".to_string(),
                )
        );
        assert!(
            repos[1].0
                == ApkRepository::new(
                    false,
                    repo_file_path_s.clone(),
                    "https://alpine.global.ssl.fastly.net/alpine/edge/testing".to_string(),
                ),
        );
        assert!(repos.len() == 2);

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_update_repo() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .update_repositories(get_message().header(), &connection)
            .await
            .unwrap();

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_search_file_owner() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .add_packages(get_message().header(), &connection, vec!["test-e"])
            .await
            .unwrap();
        assert_eq!(
            server
                .search_files_owners(vec!["/usr/bin/test-e"], DetailsFlags::all())
                .await
                .unwrap()[0],
            ApkPackagePartial::builder()
                .name("test-e".to_string())
                .version("1.0-r0".to_string())
                .description("Package E for apk-tools testsuite".to_string())
                .license("GPL".to_string())
                .url("http://alpinelinux.org".to_string())
                .installed_size(4096)
                .size(1238)
                .package_state(PackageState::Installed)
                .build(),
        );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_list_upgradable() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo.clone()), Some(root.clone()));

        server
            .add_packages(
                get_message().header(),
                &connection,
                vec!["test-a", "test-b", "test-e"],
            )
            .await
            .unwrap();

        build_additional_package("test-a-new", &repo, &root);

        let packages = server
            .list_upgradable_packages(DetailsFlags::all())
            .await
            .unwrap();

        assert_eq!(
            packages[0],
            ApkPackagePartial::builder()
                .name("test-a".to_string())
                .version("1.0-r0".to_string())
                .staging_version("1.1-r0".to_string())
                .description("Package A for apk-tools testsuite".to_string())
                .license("GPL".to_string())
                .url("http://alpinelinux.org".to_string())
                .installed_size(4096)
                .size(1254)
                .package_state(PackageState::Upgradable)
                .build(),
        );
        assert_eq!(packages.len(), 1);

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_upgrade_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo.clone()), Some(root.clone()));

        server
            .add_packages(
                get_message().header(),
                &connection,
                vec!["test-a", "test-e"],
            )
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.0\n");

        build_additional_package("test-a-new", &repo, &root);

        // No change should happen here
        server
            .upgrade_packages(get_message().header(), &connection, vec!["test-e"])
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.0\n");

        // Actually upgrade, now it should be 1.1 not 1.0
        server
            .upgrade_packages(get_message().header(), &connection, vec!["test-a"])
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.1\n");

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_get_package_details() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .add_packages(get_message().header(), &connection, vec!["test-a"])
            .await
            .unwrap();
        let pkg_a = ApkPackagePartial::builder()
            .name("test-a".to_string())
            .version("1.0-r0".to_string())
            .description("Package A for apk-tools testsuite".to_string())
            .license("GPL".to_string())
            .url("http://alpinelinux.org".to_string())
            .installed_size(4096)
            .size(1233)
            .package_state(PackageState::Installed)
            .build();
        let pkg_e = ApkPackagePartial::builder()
            .name("test-e".to_string())
            .version("1.0-r0".to_string())
            .description("Package E for apk-tools testsuite".to_string())
            .license("GPL".to_string())
            .url("http://alpinelinux.org".to_string())
            .installed_size(4096)
            .size(1254)
            .package_state(PackageState::Available)
            .build();

        let packages = server
            .get_packages_details(vec!["test-a", "test-e", "Unknown!"], DetailsFlags::all())
            .await
            .unwrap();
        assert_eq!(packages.len(), 3);
        assert_eq!(packages[0], pkg_a,);
        assert_eq!(packages[1], pkg_e,);
        assert!(packages[2].is_err());

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_delete_required_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .add_packages(get_message().header(), &connection, vec!["test-e"])
            .await
            .unwrap();
        let testa_exe_path = Path::new(&root).join("usr").join("bin").join("test-a");
        assert!(testa_exe_path.exists());
        let e = server
            .delete_packages(get_message().header(), &connection, vec!["test-a"])
            .await;
        assert!(e.is_err());
        assert_eq!(e.err().unwrap().to_string(), "dev.Cogitri.apkPolkit2.Apk: Package test-a still required by the following packages: test-b test-e");
        assert!(testa_exe_path.exists());

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_upgrade_subpackage() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo.clone()), Some(root.clone()));

        server
            .add_packages(
                get_message().header(),
                &connection,
                vec!["test-a-sub", "test-e"],
            )
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.0\n");

        build_additional_package("test-a-new", &repo, &root);

        // No change should happen here
        server
            .upgrade_packages(get_message().header(), &connection, vec!["test-e"])
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.0\n");

        // Upgrade the subpackage, that should also upgrade the main pkg
        server
            .upgrade_packages(
                get_message().header(),
                &connection,
                vec!["test-e", "test-a-sub"],
            )
            .await
            .unwrap();
        let output = Command::new(format!("{}/usr/bin/test-a", root)).output().expect("Running test-a executable failed; apparently installing the test-a package didn't work!");
        assert!(output.status.success());
        assert_eq!(output.stdout, b"hello from test-a-1.1\n");

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_replace_upgrade_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo.clone()), Some(root.clone()));

        server
            .add_packages(get_message().header(), &connection, vec!["test-a"])
            .await
            .unwrap();

        build_additional_package("test-c", &repo, &root);

        let upgrade_list = server
            .list_upgradable_packages(DetailsFlags::all())
            .await
            .unwrap();
        assert_eq!(upgrade_list.len(), 2);
        assert_eq!(
            upgrade_list[0],
            ApkPackagePartial::builder()
                .name("test-a".to_string())
                .version("1.0-r0".to_string())
                .description("Package A for apk-tools testsuite".to_string())
                .license("GPL".to_string())
                .url("http://alpinelinux.org".to_string())
                .installed_size(4096)
                .size(1229)
                .package_state(PackageState::PendingRemoval)
                .build()
        );
        assert_eq!(
            upgrade_list[1],
            ApkPackagePartial::builder()
                .name("test-c".to_string())
                .version("1.0-r1".to_string())
                .description("Package C for apk-tools testsuite".to_string())
                .license("GPL".to_string())
                .url("http://alpinelinux.org".to_string())
                .installed_size(4096)
                .size(1250)
                .package_state(PackageState::PendingInstall)
                .build()
        );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_add_bad_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        let e = server
            .add_packages(get_message().header(), &connection, vec!["test-a 1.0"])
            .await
            .err();
        assert_eq!(
                e,
                Some(apk_tools::Error::Add("Failed to add package test-a 1.0 due to error test-a 1.0 is not a correctly formated world dependency, the format should be: name(@tag)([<>~=]version)".to_string()).into())
            );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_delete_bad_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        let e = server
            .delete_packages(get_message().header(), &connection, vec!["test-a 1.0"])
            .await
            .err();
        assert_eq!(
                e,
                Some(apk_tools::Error::Delete("Failed to delete package test-a 1.0 due to error test-a 1.0 is not a correctly formated world dependency, the format should be: name(@tag)([<>~=]version)".to_string()).into())
            );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_upgrade_bad_package() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        let e = server
            .upgrade_packages(get_message().header(), &connection, vec!["test-a 1.0"])
            .await
            .err();
        assert_eq!(
                e,
                Some(apk_tools::Error::Upgrade("Failed to upgrade package test-a 1.0 due to error test-a 1.0 is not a correctly formated world dependency, the format should be: name(@tag)([<>~=]version)".to_string()).into())
            );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_get_repositories_nonexistent_file() {
        let (root, repo, temp_dir) = start_fs_test();
        let server = DBusServer::new(true, Some(repo), Some(root.clone()));
        let e = server.list_repositories().await.err();
        assert_eq!(
                e,
                Some(
                    apk_tools::Error::GetRepositories(format!(
                        "Failed to get repositories from file {}/etc/apk/repositories due to error No such file or directory (os error 2)",
                        &root
                    ))
                    .into()
                )
            );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_update_repositories_nonexistent_repo() {
        let (root, _, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(
            true,
            Some("THISFILEDOESNTEXIST".to_string()),
            Some(root.clone()),
        );
        let e = server
            .update_repositories(get_message().header(), &connection)
            .await
            .err();
        assert_eq!(format!("{}", e.unwrap()), "dev.Cogitri.apkPolkit2.Apks: An error occured: Fetching repository THISFILEDOESNTEXIST failed due to error No such file or directory".to_string());

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_database_open_nonexistent_dir() {
        let server = DBusServer::new(true, None, Some("THISFILEDOESNTEXIST".to_string()));
        let e = server
            .list_upgradable_packages(DetailsFlags::all())
            .await
            .err();
        assert_eq!(
            e,
            Some(
                apk_tools::Error::DatabaseOpen(
                    "Failed to open the apk database due to error No such file or directory"
                        .to_string()
                )
                .into()
            )
        );
    }

    #[tokio::test]
    async fn test_get_package_details_not_installed_pkg() {
        let (root, repo, temp_dir) = start_fs_test();
        let server = DBusServer::new(true, Some(repo), Some(root.clone()));
        let e = server
            .get_packages_details(vec!["unknown_package"], DetailsFlags::all())
            .await
            .unwrap()[0]
            .err();
        assert_eq!(
            e,
            "Couldn\'t retrieve details for unknown package unknown_package".to_string()
        );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_search_file_owner_nonexistent_file() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));

        server
            .add_packages(get_message().header(), &connection, vec!["test-e"])
            .await
            .unwrap();
        assert_eq!(
            server
                .search_files_owners(vec!["/usr/bin/test"], DetailsFlags::all())
                .await
                .unwrap()[0],
            ApkPackagePartial::builder().build()
        );

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_change_repository() {
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));
        let message = get_message();
        let url = "https://example.org";

        let repo_file_path = Path::new(&root)
            .join("etc")
            .join("apk")
            .join("repositories");
        create_dir_all(repo_file_path.parent().unwrap())
            .await
            .unwrap();
        let _repo_file = File::create(&repo_file_path).await.unwrap();

        server
            .add_repository(message.header().clone(), &connection, url)
            .await
            .unwrap();
        let mut repo = ApkRepository::new(
            true,
            repo_file_path.as_os_str().to_str().map(|s| s.to_string()),
            url.to_string(),
        );
        assert_eq!(server.list_repositories().await.unwrap()[0].0, repo);

        server
            .change_repository(
                message.header().clone(),
                &connection,
                url,
                ChangeRepositoryActions::Disable as i32,
            )
            .await
            .unwrap();
        repo.enabled = false;
        assert_eq!(server.list_repositories().await.unwrap()[0].0, repo);

        server
            .change_repository(
                message.header().clone(),
                &connection,
                url,
                ChangeRepositoryActions::Enable as i32,
            )
            .await
            .unwrap();
        repo.enabled = true;
        assert_eq!(server.list_repositories().await.unwrap()[0].0, repo);

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_cache() {
        use std::fs::read_dir;
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo), Some(root.clone()));
        let message = get_message();

        server
            .cache_packages(message.header(), &connection, vec!["test-a"], "", false)
            .await
            .unwrap();

        assert!(read_dir(format!("{}/var/cache/apk", root))
            .unwrap()
            .any(|file| {
                file.as_ref()
                    .unwrap()
                    .path()
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .starts_with("test-a")
            }));

        temp_dir.close().unwrap();
    }

    #[tokio::test]
    async fn test_cache_repos_file() {
        use std::fs::read_dir;
        let (root, repo, temp_dir) = start_fs_test();
        let connection = Connection::session().await.unwrap();
        let mut server = DBusServer::new(true, Some(repo.clone()), Some(root.clone()));
        let message = get_message();
        let mut repos_file = File::create(format!("{}/tmp/repositories", &root))
            .await
            .unwrap();
        let line = &repo;
        repos_file.write_all(line.as_bytes()).await.unwrap();

        server
            .cache_packages(
                message.header(),
                &connection,
                vec!["test-a"],
                &format!("{}/tmp/repositories", &root),
                false,
            )
            .await
            .unwrap();

        assert!(read_dir(format!("{}/var/cache/apk", root))
            .unwrap()
            .any(|file| {
                file.as_ref()
                    .unwrap()
                    .path()
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .starts_with("test-a")
            }));

        temp_dir.close().unwrap();
    }
}
