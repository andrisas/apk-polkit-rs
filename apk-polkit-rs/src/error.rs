use tokio::task::JoinError;

#[derive(zbus::DBusError, Debug, PartialEq)]
#[zbus(prefix = "dev.Cogitri.apkPolkit2")]
pub enum Error {
    #[zbus(error)]
    ZBus(zbus::Error),
    Apk(String),
    Apks(String),
    AppStream(String),
    Fdo(String),
    Join(String),
    InvalidChangeRepositoryAction(String),
}

pub type Result<T> = std::result::Result<T, crate::error::Error>;

impl From<apk_tools::Error> for Error {
    fn from(e: apk_tools::Error) -> Self {
        Error::Apk(e.to_string())
    }
}

impl From<apk_tools::Errors> for Error {
    fn from(e: apk_tools::Errors) -> Self {
        Error::Apks(e.to_string())
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::AppStream(e.to_string())
    }
}

impl From<zbus::fdo::Error> for Error {
    fn from(e: zbus::fdo::Error) -> Self {
        Error::Fdo(e.to_string())
    }
}

impl From<JoinError> for Error {
    fn from(e: JoinError) -> Self {
        Error::Join(e.to_string())
    }
}
