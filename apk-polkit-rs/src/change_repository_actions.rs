use crate::error::Error;

#[repr(i32)]
#[derive(Copy, Clone, Debug, PartialEq, Eq, zvariant::Type)]
pub enum ChangeRepositoryActions {
    Enable = 0,
    Disable = 1,
}

impl TryFrom<i32> for ChangeRepositoryActions {
    type Error = Error;

    fn try_from(v: i32) -> Result<Self, Self::Error> {
        match v {
            x if x == ChangeRepositoryActions::Enable as i32 => Ok(ChangeRepositoryActions::Enable),
            x if x == ChangeRepositoryActions::Disable as i32 => {
                Ok(ChangeRepositoryActions::Disable)
            }
            _ => Err(Error::InvalidChangeRepositoryAction(format!(
                "Unknown value {v}"
            ))),
        }
    }
}

#[cfg(test)]
mod test {
    use super::ChangeRepositoryActions;
    use zvariant::Type;

    #[test]
    fn signature() {
        assert_eq!(ChangeRepositoryActions::SIGNATURE, "i");
    }
}
