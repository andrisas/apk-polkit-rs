use crate::appstream_parser::*;
use apk_tools::ApkDatabase;
use log::debug;
use std::fs;
use zbus::{Connection, Proxy};

pub async fn check_upgrade_runlevel() -> Result<bool, String> {
    let connection = Connection::system().await.map_err(|e| e.to_string())?;

    let proxy = Proxy::new(
        &connection,
        "dev.andrisas.InitSystem",
        "/dev/andrisas/InitSystem",
        "dev.andrisas.InitSystem",
    )
    .await
    .map_err(|e| e.to_string())?;

    let runlevels: Vec<String> = proxy
        .get_property("CurrentRunlevels")
        .await
        .map_err(|e| e.to_string())?;
    Ok(runlevels.contains(&"upgrade".to_string()))
}

fn update_repositories(old_repos: Vec<String>, new_repos: Vec<String>) -> std::io::Result<()> {
    let original_repos = fs::read_to_string("/etc/apk/repositories")?;
    let mut updated_repos: Vec<String> = original_repos
        .lines()
        .filter(|line| !old_repos.iter().any(|repo| line.contains(repo)))
        .map(|line| line.trim().to_string())
        .collect();
    println!("{:?}", new_repos);
    updated_repos.extend(new_repos);

    fs::copy("/etc/apk/repositories", "/etc/apk/repositories.bak")?;

    fs::write("/etc/apk/repositories", updated_repos.join("\n"))?;

    Ok(())
}

pub async fn install_available_upgrades() {
    let mut db = ApkDatabase::new(false, None, None, None, false, true)
        .expect("Failed to create ApkDatabase");

    db.upgrade_world().expect("Failed to upgrade packages");

    debug!("Done upgrading packages! Updating /etc/apk/repositories");

    update_repositories(
        get_current_release_urls(&get_os_appstream_path()),
        get_latest_release_urls(&get_os_appstream_path()),
    )
    .expect("Failed to update /etc/apk/repositories");

    let connection = Connection::system()
        .await
        .expect("Failed to connect to system bus");

    connection
        .call_method(
            Some("dev.andrisas.InitSystem"),
            "/dev/andrisas/InitSystem",
            Some("dev.andrisas.InitSystem"),
            "ResetDefaultRunlevel",
            &(),
        )
        .await
        .expect("Failed to reset default runlevel");

    connection
        .call_method(
            Some("org.freedesktop.login1"),
            "/org/freedesktop/login1",
            Some("org.freedesktop.login1.Manager"),
            "Reboot",
            &(false),
        )
        .await
        .expect("Failed to reboot");
}
