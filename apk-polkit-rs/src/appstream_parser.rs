use os_release::OsRelease;
use quick_xml::events::Event;
use quick_xml::Reader;
use semver::Version;
use std::collections::HashMap;
use std::fs;
use url::Url;

pub fn get_os_appstream_path() -> String {
    let home_url = Url::parse(os_release::OsRelease::new().unwrap().home_url.as_str()).unwrap();
    let mut home_url_vec = home_url.host_str().unwrap().split(".").collect::<Vec<_>>();
    if home_url_vec.first() == Some(&"www") {
        home_url_vec.remove(0);
    }
    if home_url_vec.len() == 2 {
        home_url_vec.insert(0, home_url_vec[0]);
    }

    home_url_vec.into_iter().rev().collect::<Vec<_>>().join(".")
}

pub fn get_latest_release_urls(file: &String) -> Vec<String> {
    let repo_urls = get_repositories_urls(file);
    let highest_release = repo_urls
        .iter()
        .max_by_key(|(version, _)| version.strip_prefix("v"))
        .map(|(_, value)| value);

    (*highest_release.unwrap().clone()).to_vec()
}

pub fn get_current_release_urls(file: &String) -> Vec<String> {
    let repo_urls = get_repositories_urls(file);
    let current_release = repo_urls
        .iter()
        .find(|(version, _)| **version == OsRelease::new().unwrap().version)
        .map(|(_, value)| value);

    (*current_release.unwrap().clone()).to_vec()
}

fn get_repositories_urls(file: &String) -> HashMap<String, Vec<String>> {
    let f = fs::read_to_string(file).unwrap();
    let mut reader = Reader::from_str(&f);
    let mut buf = Vec::new();
    let mut releases: HashMap<String, Vec<String>> = HashMap::new();
    let mut curr_release = String::new();
    let mut is_repo = false;
    loop {
        buf.clear();
        match reader.read_event_into(&mut buf).unwrap() {
            Event::Eof => break,
            Event::Start(e) => match String::from_utf8_lossy(e.name().0).as_ref() {
                "release" => {
                    if let Some(version) = &e
                        .attributes()
                        .filter_map(|a| a.ok())
                        .find(|a| a.key == quick_xml::name::QName(b"version"))
                    {
                        curr_release = String::from_utf8_lossy(&version.value).to_string();
                    }
                }
                "artifact" => {
                    if let Some(a_type) = e
                        .attributes()
                        .filter_map(|a| a.ok())
                        .find(|a| a.key == quick_xml::name::QName(b"type"))
                    {
                        is_repo = String::from_utf8_lossy(&a_type.value) == "apk-repo";
                    }
                }
                "location" => {
                    if !is_repo {
                        continue;
                    }
                    if let Ok(Event::Text(e)) = reader.read_event_into(&mut buf) {
                        if let Ok(url) = e.unescape().map(|cow| cow.into_owned()) {
                            releases.entry(curr_release.clone()).or_default().push(url);
                        }
                    }
                }
                &_ => {}
            },
            _ => {}
        }
    }
    releases
}
