/*
    Copyright: 2020 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

use crate::{
    apk_arrays::{ApkBlobT, ApkDependencyArray, ApkStringArray},
    apk_package::{ApkPackage, ApkPackageError},
    apk_repository::ApkRepository,
    error::{Error, Errors},
    i18n::{i18n, i18n_f},
    PackageState,
};
use apk_tools_sys::*;
use log::{debug, info, trace, warn};
use std::{
    convert::TryFrom,
    ffi::{c_int, c_void, CStr, CString},
    fmt::Write,
    fs,
    os::raw::{c_char, c_ulong},
    path::Path,
    ptr::null_mut,
};

pub struct ApkDatabase {
    additional_repo: Option<Box<apk_repository_list>>,
    db: Box<apk_database>,
    root: Option<String>,
}

impl ApkDatabase {
    /**
     * Creates a new ApkDatabase Object and opens the DB. Keep in mind that you
     * can only have one ApkDatabase with `read_only=false` at the time. Since the DB
     * will be locked for the time you hold it, make sure to drop it ASAP.
     *
     * # Errors
     * * Returns an Error::DatabaseOpen if opening the DB fails (e.g. due to missing permissions,
     *   multiple `!read_only` instances etc.)
     */
    pub fn new(
        read_only: bool,
        additional_repo_uri: Option<String>,
        root: Option<String>,
        repositories_file: Option<String>,
        allow_untrusted_repos: bool,
        no_network: bool,
    ) -> Result<ApkDatabase, Error> {
        debug!("Creating new ApkDatabase");
        let mut db_options = apk_db_options::default();
        let mut db = Box::<apk_tools_sys::apk_database>::default();
        let additional_repo = unsafe {
            apk_db_init(db.as_mut());
            db_options.repository_list.next = &mut db_options.repository_list;
            db_options.repository_list.prev = &mut db_options.repository_list;
            if let Some(uri) = additional_repo_uri {
                trace!("Adding additional repository {}", uri);
                let mut additional_repo = Box::<apk_tools_sys::apk_repository_list>::default();
                additional_repo.url = CString::new(uri).unwrap().into_raw();
                (*db_options.repository_list.next).prev = &mut additional_repo.as_mut().list;
                additional_repo.as_mut().list.next = db_options.repository_list.next;
                additional_repo.as_mut().list.prev = &mut db_options.repository_list;
                db_options.repository_list.next = &mut additional_repo.as_mut().list;
                Some(additional_repo)
            } else {
                None
            }
        };
        if read_only {
            trace!("Opening ApkDatabase as read-only");
            db_options.open_flags = c_ulong::from(APK_OPENF_READ | APK_OPENF_NO_AUTOUPDATE);
        } else {
            trace!("Opening ApkDatabase as read-write");
            db_options.open_flags = c_ulong::from(
                APK_OPENF_READ
                    | APK_OPENF_WRITE
                    | APK_OPENF_NO_AUTOUPDATE
                    | APK_OPENF_CACHE_WRITE
                    | APK_OPENF_CREATE,
            );
        }
        if let Some(repositories_file) = repositories_file {
            if !repositories_file.is_empty() {
                trace!("Setting repositories file {}", repositories_file);
                let metadata = fs::metadata(repositories_file.clone()).unwrap();
                if metadata.len() == 0 {
                    warn!("Empty repositories file");
                }
                db_options.repositories_file = CString::new(repositories_file).unwrap().into_raw();
            }
        }
        if allow_untrusted_repos {
            warn!("Allowing untrusted repositories");
            unsafe {
                apk_flags |= APK_ALLOW_UNTRUSTED;
            }
        }
        if no_network {
            warn!("Not using network connection");
            unsafe {
                apk_flags |= APK_NO_NETWORK;
            }
        }
        if let Some(root) = root.clone() {
            trace!("Setting root {}", root);
            db_options.root = CString::new(root).unwrap().into_raw();
        }
        db_options.lock_wait = 1;

        let res = unsafe { apk_db_open(db.as_mut(), &mut db_options) };

        // deallocate db_options.root
        if !db_options.root.is_null() {
            unsafe {
                let _ = CString::from_raw(db_options.root as *mut c_char);
            };
        }

        if !db_options.repositories_file.is_null() {
            unsafe {
                drop(CString::from_raw(
                    db_options.repositories_file as *mut c_char,
                ))
            };
        }

        if res == 0 {
            trace!("Successfully opened DB");
            Ok(ApkDatabase {
                additional_repo,
                db,
                root,
            })
        } else {
            Err(Error::DatabaseOpen(i18n_f(
                "Failed to open the apk database due to error {}",
                &[&ApkDatabase::res_to_string(res)],
            )))
        }
    }

    /**
     * This sets the APK_EXTRAF_NO_CHOWN flag, meaning that apk won't attempt to chown
     * files to the `root` user. This is required for situations where this isn't run as
     * root (e.g. in tests).
     */
    pub fn no_extract_chown(&mut self) {
        warn!("Setting APK_EXTRACTF_NO_CHOWN - this should only happen in tests");
        self.db.extract_flags = APK_EXTRACTF_NO_CHOWN;
    }

    /**
     * Add (install) the packages identified by `packages`.
     *
     * # Errors
     * * Returns an `Error::Add` if any of the packages can't be added, or the
     *   transaction fails, e.g. because adding the package would introducing
     *   broken dependencies, or some package does not exist.
     */
    pub fn add_packages(&mut self, packages: Vec<&str>) -> Result<(), Error> {
        info!("Adding packages {:?}", packages);
        let mut world_copy = unsafe {
            let mut world_copy: *mut apk_dependency_array =
                apk_array_resize(std::ptr::null_mut(), 0, 0).cast();
            apk_dependency_array_copy(&mut world_copy, self.db.world);
            ApkDependencyArray(world_copy)
        };
        for pkgname in &packages {
            let mut dep = self
                .package_name_to_apk_dependency(pkgname)
                .map_err(|e| {
                    Error::Add(i18n_f(
                        "Failed to add package {} due to error {}",
                        &[pkgname, &e.to_string()],
                    ))
                })?
                .ok_or_else(|| {
                    Error::NoSuchPackage(i18n_f("Couldn't add unknown package {}", &[pkgname]))
                })?;
            unsafe {
                apk_deps_add(&mut world_copy.0, &mut dep);
            }
        }
        if unsafe { apk_solver_commit(self.db.as_mut(), 0, world_copy.0) } != 0 {
            return Err(Error::Add(i18n(
                "Failed to add packages! Please run 'apk add' for more information.",
            )));
        }
        debug!("Done adding packages {:?}", packages);
        Ok(())
    }

    /**
     * Delete (remove) the packages identified by `packages`.
     *
     * # Errors
     * * Returns an `Error::DeleteRequiredPackage` if some package can't be delete
     *   because it's still required by another package.
     * * Returns an `Error::Delete` some package can't be deleted due to some other reason.
     */
    pub fn delete_packages(&mut self, packages: Vec<&str>) -> Result<(), Error> {
        info!("Deleting packages {:?}", packages);
        let mut world_copy = unsafe {
            let mut world_copy: *mut apk_dependency_array =
                apk_array_resize(std::ptr::null_mut(), 0, 0).cast();
            apk_dependency_array_copy(&mut world_copy, (self.db).world);
            ApkDependencyArray(world_copy)
        };
        for pkgname in &packages {
            let dep = self
                .package_name_to_apk_dependency(pkgname)
                .map_err(|e| {
                    Error::Delete(i18n_f(
                        "Failed to delete package {} due to error {}",
                        &[pkgname, &e.to_string()],
                    ))
                })?
                .ok_or_else(|| {
                    Error::NoSuchPackage(i18n_f("Couldn't delete unknown package {}", &[pkgname]))
                })?;
            unsafe {
                apk_deps_del(&mut world_copy.0, dep.name);
            }
        }
        let mut changeset = apk_changeset::default();
        if unsafe { apk_solver_solve(self.db.as_mut(), 0, world_copy.0, &mut changeset) } != 0 {
            return Err(Error::Delete(i18n(
                "Failed to delete packages! Please run 'apk del' for more information.",
            )));
        }
        for i in 0..unsafe { changeset.changes.as_ref() }.unwrap().num {
            if let Some(pkg) = unsafe {
                (*changeset.changes)
                    .item
                    .as_mut_slice((*changeset.changes).num)
            }
            .get_mut(i)
            .and_then(|c| unsafe { c.new_pkg.as_mut() })
            {
                trace!(
                    "Marking package {} as potential reverse dependency during deletion",
                    unsafe { CStr::from_ptr((*pkg.name).name).to_str().unwrap() }
                );
                pkg.set_marked(1);
            }
        }
        for pkgname in &packages {
            let dependants = unsafe {
                let mut pkgname_arr: *mut apk_string_array =
                    apk_array_resize(std::ptr::null_mut(), 0, 0).cast();
                *apk_string_array_add(&mut pkgname_arr) =
                    CString::new(*pkgname).unwrap().into_raw();

                let pkgname_arr = ApkStringArray(pkgname_arr);
                let mut dependants: Vec<String> = Vec::new();

                apk_db_foreach_matching_name(
                    self.db.as_mut(),
                    pkgname_arr.0,
                    Some(ApkDatabase::foreach_matching),
                    &mut dependants as *mut _ as *mut c_void,
                );
                dependants.retain(|e| e != pkgname);
                dependants
            };

            if !dependants.is_empty() {
                return Err(Error::DeleteRequiredPackage(i18n_f(
                    "Package {} still required by the following packages: {}",
                    &[pkgname, &dependants.join(" ")],
                )));
            }
        }
        if unsafe {
            apk_solver_commit_changeset(self.db.as_mut(), &mut changeset, world_copy.0) != 0
        } {
            return Err(Error::Delete(i18n(
                "Failed to delete packages! Please run 'apk del' for more information.",
            )));
        }
        debug!("Done deleting packages {:?}", packages);
        Ok(())
    }

    /**
     * Returns a list of ApkPackage with detailed info for the package `pkgname`.
     */
    pub fn get_packages_details(
        &mut self,
        requested_packages: Vec<&str>,
    ) -> Vec<Result<ApkPackage<'_>, ApkPackageError>> {
        info!("Getting many packages details");
        let mut packages: Vec<Result<ApkPackage, ApkPackageError>> = Vec::new();
        for pkgname in requested_packages {
            info!("Trying to get details of package {}", pkgname);
            let dep = self.package_name_to_apk_dependency(pkgname);
            let dep = match dep {
                Ok(d) => match d {
                    Some(p) => p,
                    None => {
                        packages.push(Err(ApkPackageError::new(
                            pkgname.to_string(),
                            Error::NoSuchPackage(i18n_f(
                                "Couldn't retrieve details for unknown package {}",
                                &[pkgname],
                            )),
                        )));
                        continue;
                    }
                },
                Err(e) => {
                    packages.push(Err(ApkPackageError::new(
                        pkgname.to_string(),
                        Error::Details(i18n_f(
                            "Couldn't retrieve details for package {} due to error {}",
                            &[pkgname, &e.to_string()],
                        )),
                    )));
                    continue;
                }
            };
            if let Some(providers) = unsafe { dep.name.as_ref().and_then(|n| n.providers.as_ref()) }
            {
                if let Some(pkg) = providers
                    .as_slice()
                    .ok()
                    .and_then(|s| s.first())
                    .and_then(|p| unsafe { p.pkg.as_ref() })
                {
                    let pkg_state = if pkg.ipkg.is_null() {
                        PackageState::Available
                    } else {
                        PackageState::Installed
                    };
                    packages.push(Ok(ApkPackage::new(pkg, None, pkg_state)));
                } else {
                    packages.push(Err(ApkPackageError::new(
                        pkgname.to_string(),
                        Error::Details(i18n_f(
                            "Failure retrieving details for package {} -> {}",
                            &[pkgname, unsafe {
                                CStr::from_ptr(dep.name.as_ref().unwrap().name)
                                    .to_str()
                                    .unwrap()
                            }],
                        )),
                    )));
                    continue;
                }
            } else {
                packages.push(Err(ApkPackageError::new(
                    pkgname.to_string(),
                    Error::Details(i18n_f(
                        "Couldn't retrieve details for unknown package {}",
                        &[pkgname],
                    )),
                )));
                continue;
            }
        }
        packages
    }

    /**
     * Returns a list of all upgradable packages.
     *
     * # Errors
     * * Returns an `Error::ListUpgradable` if the upgrade changeset can't be calculated
     *   (e.g. because the system has a broken depgraph).
     * * Returns an `Error::IntegerOverflow` if the undyling C library, libapk, sends
     *   changesets whose size is >usize.
     */
    pub fn list_upgradable_packages(&mut self) -> Result<Vec<ApkPackage<'_>>, Error> {
        info!("Listing upgradable packages");
        let upgrade_changeset = self.get_upgrade_changeset(true, None).map_err(|e| {
            Error::ListUpgradable(i18n_f(
                "Failed to list upgradable packages due to error {}",
                &[&e.to_string()],
            ))
        })?;
        let mut packages = Vec::new();

        for i in 0..unsafe { (*upgrade_changeset.changes).num } {
            let change = unsafe { &(*upgrade_changeset.changes).as_slice()?[i] };
            if change.new_pkg.is_null() && !change.old_pkg.is_null() {
                let pkg = unsafe {
                    ApkPackage::new(
                        change.old_pkg.as_ref().unwrap(),
                        None,
                        PackageState::PendingRemoval,
                    )
                };
                trace!(
                    "Adding package {} as pending removal, to be replaced.",
                    pkg.name(),
                );
                packages.push(pkg);
            } else if !change.new_pkg.is_null() && change.old_pkg.is_null() {
                let pkg = unsafe {
                    ApkPackage::new(
                        change.new_pkg.as_ref().unwrap(),
                        None,
                        PackageState::PendingInstall,
                    )
                };
                trace!(
                    "Adding package {} as pending install, replaces another package.",
                    pkg.name(),
                );
                packages.push(pkg);
            } else if change.reinstall() != 0 {
                let pkg = unsafe {
                    ApkPackage::new(
                        change.old_pkg.as_ref().unwrap(),
                        Some(change.new_pkg.as_ref().unwrap()),
                        PackageState::Reinstall,
                    )
                };
                trace!("Reinstalling package {} (e.g. because it was rebuilt but pkgrel wasn't changed).",
                    pkg.name());
                packages.push(pkg);
            } else if !change.new_pkg.is_null() && !change.new_pkg.is_null() {
                if unsafe {
                    (apk_pkg_version_compare(change.new_pkg, change.old_pkg)
                        & (i32::try_from(APK_VERSION_LESS)?))
                        != 0
                } && change.new_pkg != change.old_pkg
                {
                    let pkg = unsafe {
                        ApkPackage::new(
                            change.old_pkg.as_ref().unwrap(),
                            Some(change.new_pkg.as_ref().unwrap()),
                            PackageState::Downgradable,
                        )
                    };
                    trace!(
                        "Adding package {} as downgradable (e.g. because the repo downgraded).",
                        pkg.name(),
                    );
                    packages.push(pkg);
                } else if unsafe {
                    (apk_pkg_version_compare(change.new_pkg, change.old_pkg)
                        & (i32::try_from(APK_VERSION_GREATER | APK_VERSION_EQUAL)?))
                        != 0
                } && change.new_pkg != change.old_pkg
                {
                    let pkg = unsafe {
                        ApkPackage::new(
                            change.old_pkg.as_ref().unwrap(),
                            Some(change.new_pkg.as_ref().unwrap()),
                            PackageState::Upgradable,
                        )
                    };
                    trace!(
                        "Adding package {} as upgradable, version {} -> {:?}",
                        pkg.name(),
                        pkg.version(),
                        pkg.staging_version(),
                    );
                    packages.push(pkg);
                } else {
                    trace!(
                        "Not adding package {} as upgradable, version compare: {}, new != old: {}",
                        unsafe {
                            CStr::from_ptr((*(*change.new_pkg).name).name)
                                .to_str()
                                .unwrap()
                        },
                        unsafe {
                            apk_pkg_version_compare(change.new_pkg, change.old_pkg)
                                & (i32::try_from(APK_VERSION_GREATER | APK_VERSION_EQUAL)?)
                        },
                        change.new_pkg != change.old_pkg
                    );
                }
            }
        }

        debug!(
            "Done listing upgradable packages. Found {} packages",
            packages.len()
        );
        Ok(packages)
    }

    /**
     * Search for the `ApkPackage` that owns `path`. Returns `None` if no package owning this package can be found.
     */
    pub fn search_file_owner(&mut self, path: &str) -> Option<ApkPackage> {
        info!("Searching for owner package of file {}", path);
        let file_path = if let Some(ref root) = self.root {
            trace!("Adding root {} to search path", root);
            let path = Path::new(path);
            if path.has_root() {
                Path::new(root).join(Path::new(path).strip_prefix("/").unwrap())
            } else {
                Path::new(root).join(path)
            }
        } else {
            Path::new(path).to_path_buf()
        };
        if let Ok(mut real_path) = std::fs::canonicalize(file_path) {
            debug!(
                "Resolved real path (w/o symlinks) to be: {:?}",
                real_path.to_str()
            );
            if let Some(root) = &self.root {
                trace!("Stripping root {} from search path", root);
                real_path = real_path.strip_prefix(root).unwrap().to_path_buf();
            }
            if let Some(path_str) = real_path.as_os_str().to_str() {
                let blob = ApkBlobT::new(path_str);
                if let Some(pkg) =
                    unsafe { apk_db_get_file_owner(self.db.as_mut(), blob.blob).as_ref() }
                {
                    let pkg = ApkPackage::new(pkg, None, PackageState::Installed);
                    debug!(
                        "Done searching for owner package of file {}. Found matching package {}",
                        path, pkg,
                    );
                    return Some(pkg);
                }
            }
        }
        debug!(
            "Done searching for owner package of file {}. Found no matching package",
            path
        );
        None
    }

    /**
     * Gets all available repositories in `$root/etc/apk/repositories`.
     *
     * # Errors
     * * Returns an `Error::GetRepositories` if opening the file or reading from it fails.
     */
    pub fn get_repositories(root: Option<&str>) -> Result<Vec<ApkRepository>, Error> {
        use std::fs::File;
        use std::io::{BufRead, BufReader};

        info!("Getting repositories");
        let repo_file_path = if let Some(root) = root {
            format!("{root}/etc/apk/repositories")
        } else {
            "/etc/apk/repositories".to_string()
        };
        let file = File::open(&repo_file_path).map_err(|e| {
            Error::GetRepositories(i18n_f(
                "Failed to get repositories from file {} due to error {}",
                &[&repo_file_path, &e.to_string()],
            ))
        })?;
        let reader = BufReader::new(file);
        let mut ret = Vec::new();

        for line in reader.lines() {
            if let Ok(line) = line.map(|l| l.trim().to_string()) {
                if !line.is_empty() {
                    trace!("Creating repository from line {}", line);
                    ret.push(ApkRepository::new(
                        !line.starts_with('#'),
                        Some(repo_file_path.clone()),
                        line.replace('#', "").trim().to_string(),
                    ));
                }
            }
        }

        debug!(
            "Done listing repositories. Found following repositories: {:?}",
            ret
        );
        Ok(ret)
    }

    /**
     * Sets the repositories `repos` in the repo file `$root/etc/apk/repositories`.
     *
     * # Errors
     * * Returns an `Error::SetRepositories` if the file can't be opened or we can't write to it.
     */
    pub async fn set_repositories(
        repos: &[ApkRepository],
        root: Option<&str>,
    ) -> Result<(), Error> {
        use tokio::{fs::File, io::AsyncWriteExt};

        info!("Setting repositories to {:?}", repos);

        let mut line = String::new();
        let repo_file_path = if let Some(root) = root {
            format!("{root}/etc/apk/repositories")
        } else {
            "/etc/apk/repositories".to_string()
        };
        let mut file = File::create(&repo_file_path).await.map_err(|e| {
            Error::SetRepositories(i18n_f(
                "Failed to set repositories from file {} due to error {}",
                &[&repo_file_path, &e.to_string()],
            ))
        })?;
        for repo in repos {
            trace!("Writing repository {} to file {}", repo, repo_file_path);
            if !repo.enabled {
                line.push('#');
            }
            writeln!(line, "{}", repo.url).unwrap();
        }
        file.write_all(line.as_bytes()).await.map_err(|e| {
            Error::SetRepositories(i18n_f(
                "Failed to set repositories from file {} due to error {}",
                &[&repo_file_path, &e.to_string()],
            ))
        })?;
        file.flush().await.map_err(|e| {
            Error::SetRepositories(i18n_f(
                "Failed to set repositories from file {} due to error {}",
                &[&repo_file_path, &e.to_string()],
            ))
        })?;

        debug!("Done setting repositories to {:?}", repos);
        Ok(())
    }

    /**
     * Updates all available repositories. Returns `false` if updating one of the repositories
     * went wrong.
     */
    pub fn update_repositories(&mut self) -> Result<(), Errors> {
        let mut err_vec = Vec::new();
        info!("Updating repositories");

        for i in APK_REPOSITORY_FIRST_CONFIGURED..self.db.num_repos {
            if i == APK_REPOSITORY_CACHED {
                continue;
            }
            if let Err(e) = self.update_repository(i) {
                err_vec.push(e);
            }
        }

        if err_vec.is_empty() {
            debug!("Successfully updated the repositories");
            Ok(())
        } else {
            Err(Errors(err_vec))
        }
    }

    /**
     * Upgrade the packages `pkgnames`.
     *
     * # Errors
     * * Returns `Error::Upgrade` if calculating the dependency tree failed, e.g. due to a broken depgraph.
     * * Returns an `Error::IntegerOverflow` if the undyling C library, libapk, sends
     *   changesets whose size is >usize.
     */
    pub fn upgrade_packages<T: AsRef<str>>(&mut self, pkgnames: &[T]) -> Result<(), Error> {
        let mut pkgnames_string = String::with_capacity(pkgnames.len() * 5);
        for pkgname in pkgnames {
            pkgnames_string.push_str(pkgname.as_ref());
            pkgnames_string.push(' ');
        }
        info!("Upgrading packages {:?}", &pkgnames_string);

        for pkgname_t in pkgnames {
            let pkgname = pkgname_t.as_ref();
            let main_dep = self
                .package_name_to_apk_dependency(pkgname.as_ref())
                .map_err(|e| {
                    Error::Update(i18n_f(
                        "Failed to upgrade package {} due to error {}",
                        &[pkgname, &e.to_string()],
                    ))
                })?
                .ok_or_else(|| {
                    Error::NoSuchPackage(i18n_f("Couldn't upgrade unknown package {}", &[pkgname]))
                })?;

            unsafe {
                apk_solver_set_name_flags(
                    main_dep.name,
                    u16::try_from(APK_SOLVERF_UPGRADE | APK_SOLVERF_AVAILABLE)?,
                    0,
                )
            };

            if let Some(origin) = unsafe {
                apk_pkg_get_installed(main_dep.name)
                    .as_ref()
                    .and_then(|p| p.origin.as_ref())
                    .and_then(|o| String::try_from(o).ok())
            } {
                trace!("Found origin {} for package {}", origin, pkgname);
                if origin != pkgname {
                    let origin_blob = ApkBlobT::new(&origin);
                    if let Some(origin_pkgname) =
                        unsafe { apk_db_query_name(self.db.as_mut(), origin_blob.blob).as_mut() }
                    {
                        debug!(
                            "Upgrading main package {} too since subpackage {} is being upgraded.",
                            origin, pkgname
                        );
                        if !unsafe { apk_pkg_get_installed(origin_pkgname) }.is_null() {
                            let origin_dep = self.package_name_to_apk_dependency(&origin)
                            .map_err(|e| {
                                Error::Update(i18n_f(
                                "Failed to upgrade subpackage {} because main package couldn't be upgraded due to error {}",
                                &[pkgname, &e.to_string()],
                            ))
                            })?
                            .ok_or_else(|| Error::NoSuchPackage(i18n_f(
                                "Couldn't upgrade unknown transistive dependency {}",
                                &[pkgname],
                            )))?;
                            unsafe {
                                apk_solver_set_name_flags(
                                    origin_dep.name,
                                    u16::try_from(APK_SOLVERF_UPGRADE | APK_SOLVERF_AVAILABLE)?,
                                    0,
                                )
                            };
                        }
                    }
                }
            }
        }
        if unsafe { apk_solver_commit(self.db.as_mut(), 0, self.db.world) } != 0 {
            return Err(Error::Upgrade(i18n_f(
                "Failed to upgrade packages {}! Please run 'apk add -u {}' for more information.",
                &[&pkgnames_string, &pkgnames_string],
            )));
        }
        debug!("Done upgrading package {}", pkgnames_string);
        Ok(())
    }

    pub fn upgrade_world(&mut self) -> Result<(), Error> {
        let mut pkgnames: Vec<&str> = Vec::new();
        for pkg in unsafe { (*self.db.world).iter_mut()? } {
            unsafe {
                pkgnames.push(CStr::from_ptr((*pkg.name).name).to_str().unwrap());
            }
        }
        self.upgrade_packages(&pkgnames)?;
        Ok(())
    }

    unsafe extern "C" fn foreach_matching(
        _: *mut apk_database,
        _: *const std::os::raw::c_char,
        name: *mut apk_name,
        ctx: *mut c_void,
    ) -> c_int {
        unsafe fn add(pkg: &apk_package, pkgnames: &mut Vec<String>) {
            if let Some(name) = pkg.name.as_ref() {
                let pkgname = CStr::from_ptr(name.name).to_str().unwrap();
                if !pkgnames.iter().any(|e| e == pkgname) {
                    pkgnames.push(pkgname.to_string());
                }
            }
            for rdepend in (*(*pkg.name).rdepends)
                .iter()
                .unwrap()
                .filter_map(|r| r.as_ref())
            {
                for pkg in (*rdepend.providers)
                    .iter()
                    .unwrap()
                    .filter_map(|p| p.pkg.as_ref())
                {
                    if !pkg.ipkg.is_null() || pkg.marked() == 1 {
                        add(pkg, pkgnames);
                    }
                }
            }
        }

        let pkgnames = ctx.cast::<Vec<String>>().as_mut().unwrap();

        if let Some(providers) = name.as_ref().and_then(|n| n.providers.as_ref()) {
            for provider in providers.as_slice().unwrap() {
                if let Some(pkg) = provider.pkg.as_ref() {
                    if pkg.marked() == 1 {
                        add(pkg, pkgnames)
                    }
                }
            }
        }
        0
    }

    fn package_name_to_apk_dependency(
        &mut self,
        pkgname: &str,
    ) -> Result<Option<apk_dependency>, Error> {
        trace!("Converting package {} to apk_dependency", pkgname);

        let mut blob = ApkBlobT::new(pkgname);
        let mut dependency = apk_dependency::default();
        unsafe {
            apk_blob_pull_dep(&mut blob.blob, self.db.as_mut(), &mut dependency);
        }
        if blob.blob.ptr.is_null() || blob.blob.len > 0 {
            return Err(Error::Dependency(i18n_f("{} is not a correctly formated world dependency, the format should be: name(@tag)([<>~=]version)", &[pkgname])));
        }
        if unsafe { (*(*dependency.name).providers).num == 0 } {
            Ok(None)
        } else {
            Ok(Some(dependency))
        }
    }

    fn update_repository(&mut self, repo_num: u32) -> Result<(), Error> {
        trace!(
            "Updating repository {}",
            unsafe { CStr::from_ptr(self.db.repos[usize::try_from(repo_num).unwrap()].url) }
                .to_str()
                .unwrap(),
        );
        let download_res = unsafe {
            apk_cache_download(
                self.db.as_mut(),
                &mut self.db.repos[usize::try_from(repo_num).unwrap()],
                null_mut(),
                i32::try_from(APK_SIGN_VERIFY)?,
                0,
                None,
                null_mut(),
            )
        };
        if download_res == 0 {
            trace!("Successfully updated repository.");
            self.db.repositories.updated += 1;
        } else if download_res != -libc::EALREADY {
            self.db.repositories.unavailable += 1;
            return Err(Error::Update(i18n_f(
                "Fetching repository {} failed due to error {}",
                &[
                    unsafe {
                        CStr::from_ptr(self.db.repos[usize::try_from(repo_num).unwrap()].url)
                            .to_str()
                            .unwrap()
                    },
                    &ApkDatabase::res_to_string(download_res),
                ],
            )));
        }
        Ok(())
    }

    fn res_to_string(res: i32) -> String {
        unsafe {
            CStr::from_ptr(apk_error_str(res))
                .to_str()
                .unwrap()
                .to_string()
        }
    }

    fn get_upgrade_changeset(
        &mut self,
        world: bool,
        additional_packages: Option<Vec<&str>>,
    ) -> Result<apk_changeset, Error> {
        trace!("Getting upgrade changeset");
        if unsafe { apk_db_check_world(self.db.as_mut(), self.db.world) } != 0 {
            return Err(Error::Upgrade(i18n(
                "Missing repository tags; can't continue the upgrade!",
            )));
        }

        let deps = unsafe {
            let mut deps: *mut apk_dependency_array =
                apk_array_resize(std::ptr::null_mut(), 0, 0).cast();
            if world {
                apk_dependency_array_copy(&mut deps, self.db.world);
            }
            if let Some(additional_packages) = additional_packages {
                for pkgname in additional_packages {
                    let mut blob: ApkBlobT = ApkBlobT::new(pkgname);
                    let mut dep: apk_dependency = apk_dependency::default();

                    apk_blob_pull_dep(&mut blob.blob, self.db.as_mut(), &mut dep);
                    *apk_dependency_array_add(&mut deps) = dep;
                }
            }
            ApkDependencyArray(deps)
        };

        for dep in unsafe { (*deps.0).iter_mut()? } {
            if dep.result_mask() == APK_DEPMASK_CHECKSUM {
                dep.set_result_mask(APK_DEPMASK_ANY);
                dep.version = std::ptr::addr_of_mut!(apk_atom_null);
            }
        }

        let mut changeset = apk_changeset::default();

        if unsafe {
            apk_solver_solve(
                self.db.as_mut(),
                u16::try_from(APK_SOLVERF_UPGRADE | APK_SOLVERF_AVAILABLE)?,
                deps.0,
                &mut changeset,
            )
        } != 0
        {
            return Err(Error::Upgrade(i18n("Failed to calculate upgrade changeset! Please run 'apk upgrade -a -s' for more information.")));
        }

        Ok(changeset)
    }

    pub fn cache_packages(&mut self, pkgnames: Vec<&str>, cache_world: bool) -> Result<(), Error> {
        let changeset: apk_changeset = self.get_upgrade_changeset(cache_world, Some(pkgnames))?;
        let mut repo: *mut apk_repository;

        for i in 0..unsafe { changeset.changes.as_ref().unwrap().num } {
            let change: &_ = unsafe { &(*changeset.changes).as_slice()?[i] };
            let pkg: *mut apk_package = change.new_pkg;
            if pkg.is_null() {
                continue;
            }

            repo = unsafe { apk_db_select_repo(self.db.as_mut(), pkg) };
            if repo.is_null() {
                continue;
            }

            let download_res =
                unsafe { apk_cache_download(self.db.as_mut(), repo, pkg, 0, 0, None, null_mut()) };

            if download_res == 0 {
                trace!("Successfully cached {}.", unsafe {
                    CStr::from_ptr((*(*pkg).name).name).to_str().unwrap()
                });
            } else if download_res != -libc::EALREADY {
                return Err(Error::Add(i18n_f(
                    "Caching package failed due to error {}",
                    &[&ApkDatabase::res_to_string(download_res)],
                )));
            }
        }
        Ok(())
    }
}

impl Drop for ApkDatabase {
    fn drop(&mut self) {
        if self.db.open_complete() == 1 {
            unsafe { apk_db_close(self.db.as_mut()) };
            if let Some(additional_repo) = &mut self.additional_repo {
                unsafe {
                    drop(CString::from_raw(additional_repo.url as *mut c_char));
                }
            }
        }
    }
}
