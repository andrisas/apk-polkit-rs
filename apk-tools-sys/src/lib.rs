/*
    Copyright: 2020-2023 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(improper_ctypes)]
#![allow(clippy::all)]

mod arrays;
mod mapk_blob_t;

pub use crate::arrays::*;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
